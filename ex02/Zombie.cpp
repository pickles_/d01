//
// Created by Dmitry Titenko on 10/31/17.
//

#include <iostream>
#include "Zombie.hpp"

void Zombie::announce()
{
	std::cout << "<" << name << " (" << type << ")> " << "Braiiiiiiinnnssss...\n";
}

const std::string &Zombie::getName() const
{
	return name;
}

void Zombie::setName(const std::string &name)
{
	Zombie::name = name;
}

const std::string &Zombie::getType() const
{
	return type;
}

void Zombie::setType(const std::string &type)
{
	Zombie::type = type;
}

Zombie::Zombie(const std::string &name, const std::string &type)
	:name(name), type(type)
{
	if (!name.empty() || !type.empty())
		std::cout << "Zombie " << name << " (" << type << ") is created." << std::endl;
}

Zombie::~Zombie()
{
	std::cout << "Zombie " << name << " (" << type << ") dies." << std::endl;
}
