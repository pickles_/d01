//
// Created by Dmitry Titenko on 10/31/17.
//

#include "ZombieEvent.hpp"

const std::string &ZombieEvent::getZombieType() const
{
	return zombieType;
}

void ZombieEvent::setZombieType(const std::string &zombieType)
{
	ZombieEvent::zombieType = zombieType;
}

Zombie *ZombieEvent::newZombie(std::string name)
{
	return new Zombie(name, zombieType);
}

void ZombieEvent::randomChump()
{
	Zombie z(ZombieEvent::randomZombieName(std::rand() % 30 + 1), zombieType);

	z.announce();
}

std::string ZombieEvent::randomZombieName(int len)
{
	std::string str;
	std::string alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	int idx;

	for (int i = 0; i < len; i++)
	{
		idx = int(std::rand() % alphabet.size()) - 1;
		str += alphabet[idx];
	}
	return str;
}
