//
// Created by Dmitry Titenko on 10/31/17.
//

#ifndef D01_ZOMBIE_HPP
#define D01_ZOMBIE_HPP


#include <string>

class Zombie
{
	private:
		std::string name;
		std::string type;
	public:
		Zombie(const std::string &name = "", const std::string &type = "");
		~Zombie();

		const std::string &getName() const;
		void setName(const std::string &name);

		const std::string &getType() const;
		void setType(const std::string &type);

		void announce();
};


#endif //D01_ZOMBIE_HPP
