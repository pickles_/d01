#include <iostream>
#include <iomanip>
#include "Zombie.hpp"
#include "ZombieEvent.hpp"

int main()
{
	Zombie *bob;
	Zombie *sam;
	ZombieEvent ev;

	std::cout << "Creating zombie with first type." << std::endl;
	ev.setZombieType("pidor");
	bob = ev.newZombie("Bob");

	std::cout << std::setfill('-') << std::setw(100) << "-" << std::endl;
	std::cout << "Creating zombie with 2nd type." << std::endl;
	ev.setZombieType("govnar'");
	sam = ev.newZombie("Sam");

	std::cout << std::setfill('-') << std::setw(100) << "-" << std::endl;
	std::cout << "Announce zombies with first and second type." << std::endl;
	bob->announce();
	sam->announce();

	std::cout << std::setfill('-') << std::setw(100) << "-" << std::endl;
	std::cout << "Random chump 1." << std::endl;
	ev.setZombieType("bastard");
	ev.randomChump();

	std::cout << std::setfill('-') << std::setw(100) << "-" << std::endl;
	std::cout << "Random chump 2." << std::endl;
	ev.setZombieType("motherfucker");
	ev.randomChump();

	std::cout << std::setfill('-') << std::setw(100) << "-" << std::endl;
	std::cout << "Kill zombies." << std::endl;
	std::cout << "Kill zombie <" << bob->getName() <<" ("<< bob->getType() <<")>." << std::endl;
	delete bob;
	std::cout << "Kill zombie <" << sam->getName() <<" ("<< sam->getType() <<")>." << std::endl;
	delete sam;
}
