//
// Created by Dmitry Titenko on 10/31/17.
//

#ifndef D01_ZOMBIEEVENT_HPP
#define D01_ZOMBIEEVENT_HPP


#include <string>
#include "Zombie.hpp"

class ZombieEvent
{
	private:
		std::string zombieType;

	public:
		const std::string &getZombieType() const;
		void setZombieType(const std::string &zombieType);

		Zombie *newZombie(std::string);
		void randomChump();
		static std::string randomZombieName(int len);
};


#endif //D01_ZOMBIEEVENT_HPP
