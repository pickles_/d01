//
// Created by Dmitry Titenko on 10/31/17.
//

#ifndef D01_HUMANB_HPP
#define D01_HUMANB_HPP


#include "HumanA.hpp"

class HumanB
{

	private:
		Weapon *weapon;
		std::string name;
	public:
		HumanB(const std::string &name);
		void attack();

		const std::string &getName() const;
		void setName(const std::string &name);

		void setWeapon(Weapon &weapon);
};


#endif //D01_HUMANB_HPP
