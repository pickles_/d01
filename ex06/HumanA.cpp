//
// Created by Dmitry Titenko on 10/31/17.
//

#include <iostream>
#include "HumanA.hpp"

const std::string &HumanA::getName() const
{
	return name;
}

void HumanA::setName(const std::string &name)
{
	this->name = name;
}

void HumanA::setWeapon(Weapon &weapon)
{
	this->weapon = weapon;
}

HumanA::HumanA(const std::string &name, Weapon &weapon)
	:weapon(weapon), name(name)
{

}

void HumanA::attack()
{
	std::cout << name << " attacks with his " << weapon.getType() << std::endl;
}
