//
// Created by Dmitry Titenko on 10/31/17.
//

#include <iostream>
#include "HumanB.hpp"

HumanB::HumanB(const std::string &name): name(name)
{}

void HumanB::attack()
{
	std::cout << name << " attacks with his " << weapon->getType() << std::endl;
}

const std::string &HumanB::getName() const
{
	return name;
}
void HumanB::setName(const std::string &name)
{
	this->name = name;
}

void HumanB::setWeapon(Weapon &weapon)
{
	this->weapon = &weapon;
}
