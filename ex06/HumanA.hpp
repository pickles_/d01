//
// Created by Dmitry Titenko on 10/31/17.
//

#ifndef D01_HUMANA_HPP
#define D01_HUMANA_HPP

#include <string>
#include "Weapon.hpp"

class HumanA
{
	private:
		Weapon &weapon;
	protected:
		std::string name;
	public:
		HumanA(const std::string &name, Weapon &weapon);

		void attack();

		const std::string &getName() const;
		void setName(const std::string &name);

		void setWeapon(Weapon &weapon);
};


#endif //D01_HUMANA_HPP
