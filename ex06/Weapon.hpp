//
// Created by Dmitry Titenko on 10/31/17.
//

#ifndef D01_WEAPON_HPP
#define D01_WEAPON_HPP


#include <string>

class Weapon
{
	private:
		std::string type;

	public:
		Weapon(const std::string &type);

		const std::string &getType() const;

		void setType(const std::string &type);
};


#endif //D01_WEAPON_HPP
