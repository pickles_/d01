//
// Created by Dmitry Titenko on 10/31/17.
//

#include "Weapon.hpp"

Weapon::Weapon(const std::string &type) : type(type)
{}

const std::string &Weapon::getType() const
{
	return type;
}

void Weapon::setType(const std::string &type)
{
	Weapon::type = type;
}
