//
// Created by Dmitry Titenko on 10/31/17.
//
#include <iostream>
#include <iomanip>
#include "Pony.hpp"

int main(void)
{

	std::cout << "In the world of wild animals of the heap." << std::endl;
	Pony::ponyOnTheHeap("Martin");
	std::cout << std::setfill('-') << std::setw(100) << "-" << std::endl;
	std::cout << "In the world of wild animals of the stack."  << std::endl;
	Pony::ponyOnTheStack("Bob");
	std::cout << std::setfill('-') << std::setw(100) << "-" << std::endl;
	std::cout << "There are no ponies left." << std::endl;
}
