//
// Created by Dmitry Titenko on 10/31/17.
//

#ifndef D01_PONY_H
#define D01_PONY_H

#include <string>

class Pony
{
	private:
		std::string name;
	public:
		const std::string &getName() const;

	public:
		Pony(std::string name);
		~Pony();
		void neigh();
		static void ponyOnTheHeap(std::string name);
		static void ponyOnTheStack(std::string name);

};


#endif //D01_PONY_H
