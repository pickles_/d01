//
// Created by Dmitry Titenko on 10/31/17.
//

#include "Pony.hpp"
#include <iostream>

Pony::Pony(std::string name)
	:name(name)
{
	std::cout << "Pony " << name << " was born." << std::endl;
}

Pony::~Pony()
{
	std::cout << "Pony " << name << " dies." << std::endl;
}

void Pony::neigh()
{
	std::cout << "Igogoggogogogogogogoggo! I'm pony " << name << ", bitch!"
			<< std::endl;
}

void Pony::ponyOnTheHeap(std::string name)
{
	Pony *pony = new Pony(name);
	std::cout << "Pony " << pony->getName() << " jumps on heap." << std::endl;
	pony->neigh();
	std::cout << "Now I'll kill pony " << pony->getName() << "." <<std::endl;
	delete pony;
	std::cout << "Pony " << pony->getName() << " dead." << std::endl;
}

void Pony::ponyOnTheStack(std::string name)
{
	Pony pony(name);
	std::cout << "Pony " << pony.getName() << " jumps on stack." << std::endl;
	pony.neigh();
	std::cout << "Pony " << pony.getName() << " will die in a few cycles." << std::endl;
}

const std::string &Pony::getName() const
{
	return name;
}
