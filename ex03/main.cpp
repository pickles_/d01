//
// Created by Dmitry Titenko on 10/31/17.
//

#include <iostream>
#include <iomanip>
#include "ZombieHorde.hpp"

int usage(char *argv0)
{
	std::cout << "Usage:" << std::endl;
	std::cout << "\t" << argv0 << " num" << std::endl;
	return (0);
}

int main(int argc, char **argv)
{
	int i;

	if (argc != 2)
		return (usage(argv[0]));
	try
	{
		i = std::stoi(std::string(argv[1]), NULL);
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
		return (usage(argv[0]));
	}

	std::cout << std::setfill('-') << std::setw(29) << "HEAP"
			  << std::setfill('-') << std::setw(26) << "-" << std::endl;
	ZombieHorde *zhh = new ZombieHorde(i);
	zhh->announce();
	delete zhh;
	std::cout << std::setfill('-') << std::setw(30) << "STACK"
			  << std::setfill('-') << std::setw(25) << "-" << std::endl;
	ZombieHorde zh(i);
	zh.announce();
}
