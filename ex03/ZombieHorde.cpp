//
// Created by Dmitry Titenko on 10/31/17.
//

#include <iostream>
#include "ZombieHorde.hpp"

std:: string randomAlphaString(int len)
{
	std::string str;
	std::string alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	int idx;

	for (int i = 0; i < len; i++)
	{
		idx = int(std::rand() % alphabet.size()) - 1;
		str += alphabet[idx];
	}
	return str;
}

ZombieHorde::ZombieHorde(int num) : num(num)
{
	zombies = new Zombie[num];

	for(int i = 0; i < num; i++)
	{
		zombies[i].setName(randomAlphaString(std::rand() % 30 + 1));
		zombies[i].setType(std::to_string(i+1));
	}
	std::cout << "All zombies was initialized." << std::endl;
}

ZombieHorde::~ZombieHorde()
{
	delete[] zombies;
	std::cout << "All zombies was destroyed." << std::endl;
}

Zombie *ZombieHorde::getZombies() const
{
	return zombies;
}

int ZombieHorde::getNum() const
{
	return num;
}

void ZombieHorde::announce()
{
	for (int i = 0; i < num; i++)
		zombies[i].announce();
}
