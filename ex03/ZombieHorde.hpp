//
// Created by Dmitry Titenko on 10/31/17.
//

#ifndef D01_ZOMBIEHORDE_HPP
#define D01_ZOMBIEHORDE_HPP


#include "Zombie.hpp"

class ZombieHorde
{
	private:
		Zombie *zombies;
		int num;

	public:
		ZombieHorde(int num);
		virtual ~ZombieHorde();

		Zombie *getZombies() const;

		int getNum() const;

		void announce();
};


#endif //D01_ZOMBIEHORDE_HPP
