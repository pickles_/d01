//
// Created by Dmitry Titenko on 10/31/17.
//

#ifndef D01_HUMAN_HPP
#define D01_HUMAN_HPP


#include <string>
#include "Brain.hpp"

class Human
{
	private:
		Brain brain;

	public:
		std::string identify();

		const Brain &getBrain() const;
};


#endif //D01_HUMAN_HPP
