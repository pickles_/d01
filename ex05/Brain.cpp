//
// Created by Dmitry Titenko on 10/31/17.
//

#include "Brain.hpp"
#include <sstream>
#include <iomanip>

std::string Brain::identify() const
{
	std::stringstream stream;

	stream << this;
	return stream.str();
}
