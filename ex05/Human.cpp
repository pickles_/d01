//
// Created by Dmitry Titenko on 10/31/17.
//

#include "Human.hpp"

const Brain &Human::getBrain() const
{
	return brain;
}

std::string Human::identify()
{
	return brain.identify();
}
