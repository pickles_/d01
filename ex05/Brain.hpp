//
// Created by Dmitry Titenko on 10/31/17.
//

#ifndef D01_BRAIN_HPP
#define D01_BRAIN_HPP


#include <string>

class Brain
{
	public:
		std::string identify() const;
};


#endif //D01_BRAIN_HPP
