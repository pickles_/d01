//
// Created by Dmitry Titenko on 11/1/17.
//

#include <iosfwd>
#include <fstream>
#include <sys/stat.h>
#include <iostream>

void errno_handler(const char *argv0, const std::string &path)
{
	std::cout << argv0 << ": " << path <<": No such file or directory" << std::endl;
}

void errno_handler(const char *argv0, const char *path)
{
	errno_handler(argv0, std::string(path));
}

void errdir_handler(const char *argv0, const char *path)
{
	std::cout << argv0 << ": " << path <<": Is a directory" << std::endl;
}

void read_file(const char *argv0, const std::string &file)
{
	std::ifstream ifile;

	ifile.open(file);
	if (!ifile.is_open())
		errno_handler(argv0, file);
	else
	{
		std::string buf((std::istreambuf_iterator<char>(ifile)),
						(std::istreambuf_iterator<char>()));
		std::cout << buf;
		ifile.close();
	}

}

void read_stdout()
{
	std::string str;
	while (!std::cin.eof())
	{
		std::getline(std::cin, str);
		std::cout << str << std::endl;
	}
}

int is_dir(char *path)
{
	struct stat path_stat;
	stat(path, &path_stat);
	return S_ISDIR(path_stat.st_mode);
}

int main(int argc, char **argv)
{
	if (argc > 1)
		for (int i = 1; i < argc; i++)
		{
			if ("-" == std::string(argv[i]))
				read_stdout();
			else if (is_dir(argv[i]))
				errdir_handler(argv[0], argv[i]);
			else
				read_file(argv[0], std::string(argv[i]));
		}
	else
		read_stdout();
}
