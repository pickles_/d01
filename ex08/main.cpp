//
// Created by Dmitry Titenko on 11/1/17.
//

#include "Human.hpp"

int main()
{
	Human human;

	human.action("is", "Shao Khan");
	human.action("ma", "Noob Saibot");
	human.action("ra", "Reptile");
}