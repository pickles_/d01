//
// Created by Dmitry Titenko on 11/1/17.
//

#ifndef D01_HUMAN_HPP
#define D01_HUMAN_HPP

#include <string>

class Human
{
	private:
		void meleeAttack(std::string const & target);
		void rangedAttack(std::string const & target);
		void intimidatingShout(std::string const & target);
	public:
		void action(std::string const & action_name, std::string const & target);
};


#endif //D01_HUMAN_HPP
