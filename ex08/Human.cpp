//
// Created by Dmitry Titenko on 11/1/17.
//

#include <iostream>
#include "Human.hpp"



struct Action
{
	std::string action_name;
	void (Human::*func)(std::string const &target);
};

void Human::meleeAttack(std::string const &target)
{
	std::cout << "Attacks " << target << " with a melee attack" << std::endl;
}

void Human::rangedAttack(std::string const &target)
{
	std::cout << "Attacks " << target << " with a ranged attack" << std::endl;
}

void Human::intimidatingShout(std::string const &target)
{
	std::cout << "Attacks " << target << " with a intimidating shout" << std::endl;
}

void Human::action(std::string const &action_name, std::string const &target)
{
	Action a[] = {
			{"is", &Human::intimidatingShout},
			{"ra", &Human::rangedAttack},
			{"ma", &Human::meleeAttack}
	};
	for (int i = 0; i < 3; i++)
		if (action_name == a[i].action_name)
		{
			(this->*a[i].func)(target);
			break;
		}
}
