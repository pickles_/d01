//
// Created by Dmitry Titenko on 11/1/17.
//

#include <iostream>
#include <iomanip>
#include <sstream>
#include "Logger.hpp"

struct Destination
{
	std::string dest_name;
	void (Logger::*logFunc)(const std::string &entry);
};

Logger::Logger(const std::string &logfilename)
{
	logfile.open(logfilename, std::ios::app);
}

Logger::~Logger()
{
	logfile.close();
}

void Logger::log(std::string const &dest, std::string message)
{
	Destination dests[2] = {
		{"stdout", &Logger::logToConsole},
		{"file", &Logger::logToFile}
	};
	for (int i  = 0; i < 2; i++)
		if (dest == dests[i].dest_name)
		{
			(this->*dests[i].logFunc)(makeLogEntry(message));
			break ;
		}
}

void Logger::logToConsole(const std::string &entry)
{
	std::cout << entry << std::endl;
}

void Logger::logToFile(const std::string &entry)
{
	if (logfile.is_open())
		logfile << entry << std::endl;
	else
		logToConsole(entry);
}

std::string Logger::makeLogEntry(const std::string &message)
{
	time_t t = std::time(nullptr);
	struct tm tm = *std::localtime(&t);

	std::stringstream ss;
	ss << std::put_time(&tm, "%d-%m-%Y\t%H-%M-%S:\t\t");
	std::string str = ss.str();
	return str + message;
}
