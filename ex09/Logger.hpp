//
// Created by Dmitry Titenko on 11/1/17.
//

#ifndef D01_LOGGER_HPP
#define D01_LOGGER_HPP


#include <string>
#include <fstream>

class Logger
{
	public:
		Logger(const std::string &logfilename);
		~Logger();
		void log(std::string const &dest, std::string);

	private:
		std::ofstream logfile;
		void logToConsole(const std::string &);
		void logToFile(const std::string &);
		std::string makeLogEntry(const std::string &message);

};

#endif //D01_LOGGER_HPP
