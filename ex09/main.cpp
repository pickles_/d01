//
// Created by Dmitry Titenko on 11/1/17.
//

#include "Logger.hpp"

int main()
{
	Logger logger("test.log");

	logger.log("stdout", "log to stdout.");
	logger.log("file", "log to file.");
	logger.log("", "log to nowhere!");

	logger.log("stdout", "new log string in stdout.");
	logger.log("file", "hahaha, it's working");
}
