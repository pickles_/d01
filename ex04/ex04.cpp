//
// Created by Dmitry Titenko on 10/31/17.
//

#include <string>
#include <iostream>

int main()
{
	std::string	string = "HI THIS IS BRAIN";
	std::string	*strPtr = &string;
	std::string	&strRef = string;

	std::cout << *strPtr << std::endl;
	std::cout << strRef << std::endl;
}
