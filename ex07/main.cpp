//
// Created by Dmitry Titenko on 10/31/17.
//

#include <iostream>
#include <fstream>

void errno_print(std::string fname)
{
	std::cout << "Error: no such file or directory: " << fname
			  << std::endl;
	exit(EXIT_FAILURE);
}

void capitalize(std::string &str)
{
	for (size_t i = 0; i < str.size(); i++)
		str[i] = char(std::toupper(str[i]));

}

int usage(char *argv0)
{
	std::cout << "Usage:" << std::endl;
	std::cout << "\t" << argv0 << "\tfilepath str1 str2" << std::endl;
	return (0);
}

void replace(const std::string &filepath,
			 const std::string &str1,
			 const std::string &str2)
{
	std::ofstream ofile;
	std::ifstream ifile;
	std::string newfilename;
	std::streampos s;

	//Create name for new file
	newfilename = filepath;
	capitalize(newfilename);
	newfilename += ".replace";

	//Open streams to files
	ifile.open(filepath);
	ofile.open(newfilename, std::ios::trunc | std::ios::out);
	if (!ifile.is_open())
		errno_print(filepath);
	else if (!ofile.is_open())
		errno_print(newfilename);

	//read all to string
	std::string buf((std::istreambuf_iterator<char>(ifile)),
					(std::istreambuf_iterator<char>()));

	//Replace while can;
	size_t pos;
	pos = 0;
	while ((pos = buf.find(str1, pos)) != std::string::npos)
	{
		buf.replace(pos, str1.size(), str2);
		pos += str2.size();
	}

	//Output to file
	ofile << buf;

	//Close streams
	ofile.close();
	ifile.close();
}

int main(int argc, char **argv)
{
	if (argc != 4)
	{
		std::cout << "Error: invalid number of arguments." << std::endl;
		return (usage(argv[0]));
	}
	std::string filename(argv[1]), str1(argv[2]), str2(argv[3]);
	replace(std::string(argv[1]), std::string(argv[2]), std::string(argv[3]));
}
